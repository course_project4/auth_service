create user 'auth_user'@'%' identified by 'Auth123';

create database auth;

GRANT ALL PRIVILEGES ON auth.* TO 'auth_user'@'%';

USE auth;

create table if not exists `user` (
    id int not null auto_increment primary key,
    user_id varchar(255) not null unique,
    password varchar(255) not null
);

insert into `user` (user_id, password) values('raximvarresult@gmail.com', 'Admin123')