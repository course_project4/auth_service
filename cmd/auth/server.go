package main

import (
	"database/sql"
	"fmt"
	"log"
	"net"

	auth "auth_service/internal/implementation"
	pb "auth_service/proto"

	_ "github.com/go-sql-driver/mysql"
	"google.golang.org/grpc"
)

const (
	dbDriver = "mysql"
	dbName   = "auth"
)

var db *sql.DB

func main() {
	var err error

	dbUser := "auth_user"
	dbPassword := "Auth123"
	// Open a database connection
	dsn := fmt.Sprintf("%s:%s@tcp(mysql-auth:3306)/%s", dbUser, dbPassword, dbName)
	db, err := sql.Open(dbDriver, dsn)
	if err != nil {
		log.Fatal(err)
	}

	defer func() {
		if err := db.Close(); err != nil {
			log.Printf("Error closing db: %s", err)
		}
	}()

	if err := db.Ping(); err != nil {
		log.Fatalf("Error pinging to the db: %v", err)
	}
	log.Println("pinged successfully")

	// grpc ledger setup
	grpServer := grpc.NewServer()
	authServiceImplementation := auth.NewAuthImplementation(db)
	pb.RegisterAuthServiceServer(grpServer, authServiceImplementation)

	// listen and serve
	listen, err := net.Listen("tcp", ":9000")
	if err != nil {
		log.Fatalf("failed to listen on port 9000 %v", err)
	}

	log.Printf("Server is listening at %s", listen.Addr())

	if err := grpServer.Serve(listen); err != nil {
		log.Fatalf("failed grpServer.Serve %v", err)
	}
}
