package auth

import (
	"context"
	"database/sql"
	"errors"
	"log"
	"os"
	"time"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	jwt "github.com/golang-jwt/jwt/v5"

	pb "auth_service/proto"
)

type Implementation struct {
	db *sql.DB
	pb.UnimplementedAuthServiceServer
}

func NewAuthImplementation(db *sql.DB) *Implementation {
	return &Implementation{db: db}
}

func (i *Implementation) GetToken(ctx context.Context, credentials *pb.Credentials) (*pb.Token, error) {
	type User struct {
		userID   string
		password string
	}

	var u User
	stmt, err := i.db.Prepare("select user_id, password from user where user_id=? and password=?")
	if err != nil {
		log.Printf("error i.db.Prepare %v", err)
		return nil, err
	}

	log.Println("credentials.GetUserName(): ", credentials.GetUserName())

	if err := stmt.QueryRow(credentials.GetUserName(), credentials.GetPassword()).
		Scan(&u.userID, &u.password); err != nil {
		log.Printf("error stmt.QueryRow %v", err)
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Error(codes.Unauthenticated, err.Error())
		}
		return nil, status.Error(codes.Internal, err.Error())
	}

	jwtToken, err := createJWT(u.userID)
	if err != nil {
		return nil, err
	}
	return &pb.Token{Jwt: jwtToken}, nil
}

func (i *Implementation) ValidateToken(ctx context.Context, credentials *pb.Token) (*pb.User, error) {
	key := []byte(os.Getenv("SIGNING_KEY"))
	log.Println("ValidateToken SIGNING_KEY and credentials.Jwt: ", string(key), credentials.Jwt)
	userID, err := validateJWT(credentials.Jwt, key)
	if err != nil {
		return nil, err
	}
	log.Println("userID: ", userID)
	return &pb.User{UserID: userID}, nil
}

func createJWT(userID string) (string, error) {
	key := []byte(os.Getenv("SIGNING_KEY"))
	log.Println("SIGNING_KEY: ", string(key))
	now := time.Now()
	token := jwt.NewWithClaims(jwt.SigningMethodHS256,
		jwt.MapClaims{
			"iss": "auth-service",
			"sub": userID,
			"iat": now.Unix(),
			"exp": now.Add(time.Hour).Unix(),
		})

	signedToken, err := token.SignedString(key)
	if err != nil {
		log.Printf("error i.db.Prepare %v", err)
		return "", status.Error(codes.Internal, err.Error())
	}
	return signedToken, nil
}

func validateJWT(t string, key []byte) (string, error) {
	type MyClaims struct {
		jwt.RegisteredClaims
	}

	parsedToken, err := jwt.ParseWithClaims(t, &MyClaims{}, func(token *jwt.Token) (interface{}, error) {
		return key, nil
	})
	if err != nil {
		log.Println("jwt err1: ", err)
		if errors.Is(err, jwt.ErrTokenExpired) {
			return "", status.Error(codes.Unauthenticated, "token expired")
		}
		return "", status.Error(codes.Internal, err.Error())
	}

	claims, ok := parsedToken.Claims.(*MyClaims)
	if !ok {
		log.Println("parsedToken.Claims: ", err)
		return "", status.Error(codes.Internal, "type assertion failed")
	}
	log.Println("claims: ", claims)
	return claims.RegisteredClaims.Subject, nil
}
